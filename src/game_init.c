#include <gb/gb.h>

#include "game_init.h"
#include "cursor.h"

void game_init()
{
  cursor_position_x = 0;
  cursor_position_y = 0;
}
