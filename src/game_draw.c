#include <gb/gb.h>

#include "game_draw.h"
#include "cursor.h"

void game_draw()
{
  move_sprite(0, (cursor_position_x + 1) << 3, (cursor_position_y + 4) << 3);
}
