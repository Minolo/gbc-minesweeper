#include <gb/gb.h>

#include "game_update.h"
#include "cursor.h"

void game_update(UINT8 joypad_state_current, UINT8 joypad_state_pressed)
{
  // No warning pls
  joypad_state_current = 0;

  // Update cursor position
  if(joypad_state_pressed & J_RIGHT)
  {
    if(cursor_position_x < 19)
    {
      cursor_position_x++;
    }
  }

  if(joypad_state_pressed & J_LEFT)
  {
    if(cursor_position_x > 0)
    {
      cursor_position_x--;
    }
  }

  if(joypad_state_pressed & J_UP)
  {
    if(cursor_position_y > 0)
    {
      cursor_position_y--;
    }
  }

  if(joypad_state_pressed & J_DOWN)
  {
    if(cursor_position_y < 15)
    {
      cursor_position_y++;
    }
  }
}
