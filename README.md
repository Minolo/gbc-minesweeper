# gbc-minesweeper

gbc-minesweeper is a Minesweeper implementation for the Game Boy Color console.

It was created with the following goals in mind:
  - To deepen my knowledge of the C language and the Make utility.
  - To develop a project for a resource-limited platform.
  - To have fun!!1

# Compilation
This project uses the GBDK library. To install it in an Arch Linux environment, you can use [this AUR package](https://aur.archlinux.org/packages/gbdk/).

Add the following line to your .bashrc:
```sh
export SDK_DIR=/opt/gbdk/
```

After that, just run this in your repository folder:
```sh
$ make run
```

That will compile the project and run the emulator. By default it uses the [mgba-qt](https://www.archlinux.org/packages/community/x86_64/mgba-qt/) emulator, but you can edit which one to use in the Makefile.
