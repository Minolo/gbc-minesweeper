#ifndef GAME_UPDATE_H
#define GAME_UPDATE_H

void game_update(UINT8 joypad_state_current, UINT8 joypad_state_pressed);

#endif //GAME_UPDATE_H
