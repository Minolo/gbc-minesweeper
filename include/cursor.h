/* Standard prologue */
#if defined(DEFINE_VARIABLES) && !defined(CURSOR_H_DEFINITIONS)
#undef CURSOR_H
#endif

#ifndef CURSOR_H
#define CURSOR_H

#include "extern.h"   /* Support macro EXTERN */

#if !defined(DEFINE_VARIABLES) || !defined(CURSOR_H_DEFINITIONS)

/* Global variable declarations / definitions */
EXTERN UINT8 cursor_position_x;
EXTERN UINT8 cursor_position_y;

#endif /* !DEFINE_VARIABLES || !CURSOR_H_DEFINITIONS */

/* Standard epilogue */
#ifdef DEFINE_VARIABLES
#define CURSOR_H_DEFINITIONS
#endif /* DEFINE_VARIABLES */

#endif /* CURSOR_H */
