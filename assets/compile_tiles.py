import sys
import argparse

from PIL import Image

##############################################################################
# CLASSES
##############################################################################
# Internal exception for image errors
class ImageFormatError(Exception):
    pass

# Image info class
class ImageInfo:
    def __init__(self, data, palette, num_colors, compatible_palette_index):
        self.data = data
        self.palette = palette
        self.num_colors = num_colors
        self.compatible_palette_index = compatible_palette_index

    def useful_colors(self):
        return self.palette[-self.num_colors:]

##############################################################################
# HELPER FUNCTIONS
##############################################################################
# Get hex representation of an array containing 8 zeroes or ones
def hex_from_binary_array(array):
    binary = "0b" + "".join([str(n) for n in array])
    return "0x{:02X}".format(int(binary, 2))

# Convert RGB values to grayscale
def rgb_to_grayscale(r, g, b):
    return (r * 0.299 + g * 0.587 + b * 0.114)

# Check if a palette is compatible with another one, that is, if all the colors of the first one
# are contained in the second one
def is_palette_compatible(palette1, palette2):
    return set(palette1).issubset(set(palette2))

# Look for a palette that is compatible with the given one and return its index
def find_first_compatible_palette_index(images_info, palette):
    return next(index for index in range(len(images_info)) if is_palette_compatible(palette, images_info[index].palette))

# Open an image file, returning a tuple containing (image data, palette, number of colors in the image, 0)
def open_image(filename):
    # Open image
    image = Image.open(filename)

    # Check image size (for now only 8x8 images are supported)
    if image.height != 8 or image.width != 8:
        raise ImageFormatError("The image {} has incorrect size".format(filename))

    # Collect colors
    colors = [color for (count, color) in image.getcolors()]

    # Separate opaque colors and alpha colors
    colors_opaque = list(filter(lambda c : len(c) == 3 or c[3] > 128, colors))
    colors_alpha = list(filter(lambda c : c not in colors_opaque, colors))

    # Check that there at most 4 colors
    if len(colors_opaque) > 4 or (len(colors_opaque) == 4 and len(colors_alpha) > 0):
        raise ImageFormatError("The image {} has too many colors".format(filename))

    # Sort by grayscale
    colors_opaque = sorted(colors_opaque, key = lambda c: rgb_to_grayscale(c[0], c[1], c[2]))

    # Get image data, replacing alpha colors
    data = [((0,0,0) if color in colors_alpha else color[:3]) for color in image.getdata()]

    # Build palette, stripping opaque colors alpha channel
    palette = [(0,0,0)] * (4 - len(colors_opaque)) + [color[:3] for color in colors_opaque]

    # Return data
    return ImageInfo(data, palette, len(set(data)), 0)

# Create the GB tile for the given data and palette
def create_gb_tile(data, palette, verbose):
    # Create tile
    tile = [palette.index(color) for color in data]

    # Get tile info in the format the Game Boy uses
    tile_upper = [(n & 2) // 2 for n in tile]
    tile_lower = [(n & 1)      for n in tile]

    tile_upper_split = list(zip(*[iter(tile_upper)]*8))
    tile_lower_split = list(zip(*[iter(tile_lower)]*8))

    tile_upper_grouped = [hex_from_binary_array(l) for l in tile_upper_split]
    tile_lower_grouped = [hex_from_binary_array(l) for l in tile_lower_split]

    tile_final = [item for pair in zip(tile_lower_grouped, tile_upper_grouped) for item in pair]

    # Print debug info
    if verbose:
        print("Tile: {}".format(tile))
        print("Palette: {}".format(palette))
        print("GB tile: {}".format(",".join(tile_final)))
        print()

    return tile_final

##############################################################################
# MAIN
##############################################################################
def main():
    # Define arguments in the parser
    parser = argparse.ArgumentParser(description = "Generate a C file containing tile definitions from several input PNG images")

    parser.add_argument("-v", "--verbose", action="store_true", help="enables verbose mode")
    parser.add_argument("output_file_prefix", type=str, help="the output header/C file name prefix (the final file names will end in .h/.c respectively)")
    parser.add_argument("tile_prefix", type=str, help="the prefix for the tile group, appearing in defined constant names and some other places")
    parser.add_argument("template_file", type=str, help="the template file to use when generating the header and code files")
    parser.add_argument("input_file", type=str, help="the input image file[s]", nargs="+")

    # Get arguments
    args = parser.parse_args()

    verbose = args.verbose
    output_file_prefix = args.output_file_prefix
    tile_prefix = args.tile_prefix
    template_file = args.template_file
    input_files = args.input_file

    # Open images
    images_info = [open_image(filename) for filename in input_files]

    # Sort by number of colors
    images_info_sorted = sorted(images_info, key = lambda p: p.num_colors, reverse = True)

    # Add compatible palette information
    compatible_palette_indices = [find_first_compatible_palette_index(images_info_sorted, image_info.useful_colors()) for image_info in images_info_sorted]

    # Create tiles
    gb_tiles = [create_gb_tile(image_info.data, images_info_sorted[palette_index].palette, verbose) for (image_info, palette_index) in zip(images_info_sorted, compatible_palette_indices)]

    # TODO

##############################################################################
if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        quit("[{}] {}".format(str(type(e).__name__), str(e)))
