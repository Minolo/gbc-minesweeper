# Directory configuration
SRC_DIR = src
INCLUDE_DIR = include
ASSETS_DIR = assets

OUTPUT_DIR = bin
OBJ_DIR = obj

# Compiler and emulator configuration
CC = lcc -Wa-l -Wl-m -Wl-j
CFLAGS 	= -I$(INCLUDE_DIR)
GBC_FLAGS = -Wl-yp0x143=0x80
EMULATOR = mgba-qt

# Target
TARGET = $(OUTPUT_DIR)/minesweeper.gbc

# Collect .c files
SOURCE_FILES = $(wildcard $(SRC_DIR)/*.c)
OBJECT_FILES = $(patsubst %.c,$(OBJ_DIR)/%.o,$(notdir $(SOURCE_FILES)))

# Create output directory for the current target
MAKE_DIRECTORY = @mkdir -p $(@D)

# Define phony targets
.PHONY: all run clean

# Default target
all: $(TARGET)

# Don't delete obj files after compilation
.PRECIOUS: $(OBJ_DIR)/%.o

# Compile assembly files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.s
	$(MAKE_DIRECTORY)
	$(CC) -c -o $@ $<

# Compile C source files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(MAKE_DIRECTORY)
	$(CC) $(CFLAGS) -c -o $@ $<

# Link object files to final binary
$(TARGET): $(OBJECT_FILES)
	$(MAKE_DIRECTORY)
	$(CC) $(GBC_FLAGS) -o $@ $^

# Run the emulator
run:	all
	$(EMULATOR) $(TARGET)

# Clean target
clean:
	rm -f $(OUTPUT_DIR)/*.{gb,gbc,sav}
	rm -f $(OBJ_DIR)/*.{o,lst,map,~,rel,cdb,ihx,lnk,sym,asm}
